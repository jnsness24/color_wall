
var divHtml = '<div class="trigger-flip-container"><div class="inner-flip-container front-flipped"></div></div>';

var singleTriggerFlipContainer = $('.trigger-flip-container');

for (var i = 0; i < 200; i++) {
    singleTriggerFlipContainer.after(divHtml);
}
var triggerFlipContainer = $('.trigger-flip-container');


triggerFlipContainer.mouseover(function () {
    var randomColor = getRandomColor();
    $('.inner-flip-container', this).css({
        "transform":"rotateY(180deg)",
        "background-color":randomColor
    });
});

triggerFlipContainer.mouseout(function () {
    $('.inner-flip-container', this).css("transform","rotateY(0deg)");
});

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}
